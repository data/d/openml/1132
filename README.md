# OpenML dataset: AP_Omentum_Lung

https://www.openml.org/d/1132

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

GEMLeR provides a collection of gene expression datasets that can be used for benchmarking gene expression oriented machine learning algorithms. They can be used for estimation of different quality metrics (e.g. accuracy, precision, area under ROC curve, etc.) for classification, feature selection or clustering algorithms.

This repository was inspired by an increasing need in machine learning / bioinformatics communities for a collection of microarray classification problems that could be used by different researches. This way many different classification or feature selection techniques can finally be compared to eachother on the same set of problems.

Origin of data

Each gene expression sample in GEMLeR repository comes from a large publicly available expO (Expression Project For Oncology) repository by International Genomics Consortium.

The goal of expO and its consortium supporters is to procure tissue samples under standard conditions and perform gene expression analyses on a clinically annotated set of deidentified tumor samples. The tumor data is updated with clinical outcomes and is released into the public domain without intellectual property restriction. The availability of this information translates into direct benefits for patients, researchers and pharma alike.

Source: expO website
Although there are various other sources of gene expression data available, a decision to use data from expO repository was made because of:
- consistency of tissue samples processing procedure
- same microarray platform used for all samples
- availability of additional information for combined genotype-phenotype studies
- availability of a large number of samples for different tumor types

In case of publishing material based on GEMLeR datasets, then, please note the assistance you received by using this repository. This will help others to obtain the same datasets and replicate your experiments. Please cite as follows when referring to this repository:

Stiglic, G., & Kokol, P. (2010). Stability of Ranked Gene Lists in Large Microarray Analysis Studies. Journal of biomedicine biotechnology, 2010, 616358.

You are also welcome to acknowledge the contribution of expO (Expression Project For Oncology) and International Genomics Consortium for providing their gene expression samples to the public.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1132) of an [OpenML dataset](https://www.openml.org/d/1132). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1132/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1132/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1132/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

